# Focus Telecom Polska

### Zadanie rekrutacyjne numer 2

Napisz program, używając biblioteki asyncio, który będzie nakładał na zadany tekst odpowiednie filtry. Program powininen posiadać interfejs wejściowy w postaci serwera HTTP, na który będą wysyłane requesty z danymi mówiącymi o tym:

- jaki tekst będzie przetwarzany
- jakie filtry będą na tekst nakładane
- na jaki adres URL odsyłać informację o postępie przetwarzania (opcjonalny parametr)

np:


```json
{
"text": "jestem tekstem do przetworzenia",
"filters": ["capitalize", "upper", "lower"],
"progress_url": "http://jestembardzofajnymadresem.pl/jestemenpointem"
}
```

Możliwe filtry:

- capitalize (pierwsza litera zamieniona na dużą, reszta na małe)
- lower (wszystkie znaki zamienione na małe)
- title (pierwszy znak każdego wyrazu zamieniony na dużą literę, reszta liter na małe)
- upper (wszystkie znaki zamienione na duże)
- stragne_format (wielkość znaków się naprzemiennie zmienia np. jestem testem -> jEsTeM tEsTeM


po każdym zaaplikowanym filtrze program ma wysłać żądanie HTTP POST z rezultatem przetwarzania na adres podany w danych wejściowch. O ile oczywiście został on podany. W czasie developmentu można użyć np. https://webhook.site/ do testowania.  

Kolejność aplikowania filtrów ma znaczenie i musi być zachowana. 

Historia oraz status (InProgress/Done) aktualnie przetwarzanych elementów powinien znajdować się w bazie danych (wybór bazy należy do kandydata). Te dane powinny być dostępne do wglądu pod endpointem `/status` w postaci prostej tabelki HTML.

Wymagania pozafunkcjonalne:

- aplikacja ma gromadzić logi ze swojej pracy
- aplikacja powinna być otestowana
- aplikacja powinna mieć informację jak ją skonfigurować i jak uruchomić
- aplikacja powinna mieć swój własny plik konfiguracyjny gdzie będą przechowywane np. dane dostępowe do bazy danych

Fajnie by było, aby w repo pojawiło sie Dockerfile/docker-compose pozwalający uruchomić aplikację.  

